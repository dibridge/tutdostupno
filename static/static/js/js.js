/**
 * Created by max on 23.05.17.
 */
$(document).ready(function() {
    $(function(){
      $(".phone-check").mask("+38099 999 99 99");
    });
  var $slider = $(".slider"),
      $slideBGs = $(".slide__bg"),
      diff = 0,
      curSlide = 0,
      numOfSlides = $(".slide").length-1,
      animating = false,
      animTime = 500,
      autoSlideTimeout,
      autoSlideDelay = 6000,
      $pagination = $(".slider-pagi");

  function createBullets() {
    for (var i = 0; i < numOfSlides+1; i++) {
      var $li = $("<li class='slider-pagi__elem'></li>");
      $li.addClass("slider-pagi__elem-"+i).data("page", i);
      if (!i) $li.addClass("active");
      $pagination.append($li);
    }
  }
    createBullets();

  function manageControls() {
    $(".slider-control").removeClass("inactive");
    if (!curSlide) $(".slider-control.left").addClass("inactive");
    if (curSlide === numOfSlides) $(".slider-control.right").addClass("inactive");
  }
    function autoSlide() {
    autoSlideTimeout = setTimeout(function() {
      curSlide++;
      if (curSlide > numOfSlides) curSlide = 0;
      changeSlides();
    }, autoSlideDelay);
  }
    autoSlide();

  function changeSlides(instant) {
    if (!instant) {
      animating = true;
      manageControls();
      $slider.addClass("animating");
      $slider.css("top");
      $(".slide").removeClass("active");
      $(".slide-"+curSlide).addClass("active");
      setTimeout(function() {
        $slider.removeClass("animating");
        animating = false;
      }, animTime);
    }
    window.clearTimeout(autoSlideTimeout);
    $(".slider-pagi__elem").removeClass("active");
    $(".slider-pagi__elem-"+curSlide).addClass("active");
    $slider.css("transform", "translate3d("+ -curSlide*100 +"%,0,0)");
    $slideBGs.css("transform", "translate3d("+ curSlide*50 +"%,0,0)");
    diff = 0;
    autoSlide();
  }

  function navigateLeft() {
    if (animating) return;
    if (curSlide > 0) curSlide--;
    changeSlides();
  }

  function navigateRight() {
    if (animating) return;
    if (curSlide < numOfSlides) curSlide++;
    changeSlides();
  }

  $(document).on("mousedown touchstart", ".slider", function(e) {
    if (animating) return;
    window.clearTimeout(autoSlideTimeout);
    var startX = e.pageX || e.originalEvent.touches[0].pageX,
        winW = $(window).width();
    diff = 0;

    $(document).on("mousemove touchmove", function(e) {
      var x = e.pageX || e.originalEvent.touches[0].pageX;
      diff = (startX - x) / winW * 70;
      if ((!curSlide && diff < 0) || (curSlide === numOfSlides && diff > 0)) diff /= 2;
      $slider.css("transform", "translate3d("+ (-curSlide*100 - diff) +"%,0,0)");
      $slideBGs.css("transform", "translate3d("+ (curSlide*50 + diff/2) +"%,0,0)");
    });
  });

  $(document).on("mouseup touchend", function(e) {
    $(document).off("mousemove touchmove");
    if (animating) return;
    if (!diff) {
      changeSlides(true);
      return;
    }
    if (diff > -8 && diff < 8) {
      changeSlides();
      return;
    }
    if (diff <= -8) {
      navigateLeft();
    }
    if (diff >= 8) {
      navigateRight();
    }
  });

  $(document).on("click", ".slider-control", function() {
    if ($(this).hasClass("left")) {
      navigateLeft();
    } else {
      navigateRight();
    }
  });

  $(document).on("click", ".slider-pagi__elem", function() {
    curSlide = $(this).data("page");
    changeSlides();
  });

    $(document).on("click", ".arrow-menu", function (e) {
            if ($('.menu').hasClass('visible-md')){
                $('.menu').removeClass('visible-md').removeClass('visible-lg').slideDown();
                $('#label').addClass('hidden');
                $('.main-down').addClass('hidden');
                $("input[name=\'filter_check\']").removeAttr("checked");
            }
            else{
                $('.menu').addClass('visible-md').addClass('visible-md').slideUp();
                $('#label').removeClass('hidden');
                $('.main-down').removeClass('hidden');
            }
        });
     $(document).on("click", "#label", function (e){
            console.log('ddd');
            if ($('.main-down').hasClass('hidden')){
                $('.main-down').removeClass('hidden');
            }
            else{
                 $('.main-down').addClass('hidden');
            }
        });
//    Это наше верхнее меню на телефоне !!!!!!!!!!!
    var delay = 1000; // Задержка прокрутки
    $(window).scroll(function () {
          if ($(this).scrollTop() > 100) {
              console.log('dd');
              $('.top-mob').addClass('fixed-menu-arr');
              $('.menu').addClass('fixed-menu');
          }
          else if ($(this).scrollTop() < 100) {
              $('.top-mob').removeClass('fixed-menu-arr');
              $('.menu').removeClass('fixed-menu');

          }
          if ($(this).scrollTop() > 200) $('#top').fadeIn();
          else $('#top').fadeOut();
    });
    $('.go_to').click( function(){ // ловим клик по ссылке с классом go_to
	var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
	    $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500); // анимируем скроолинг к элементу scroll_el
        }
	    return false; // выключаем стандартное действие
    });
    // В каком положении полосы прокрутки начинать показ кнопки "Наверх"
    var delay = 1000; // Задержка прокрутки
      $(document).ready(function() {
          $(window).scroll(function () {
              var dst_prod = $('#prod').offset().top - 300;// При прокрутке попадаем в эту функцию
              if ($(this).scrollTop() > 300){
                  $('.main-down').fadeIn();
              }
              else if ($(this).scrollTop() < 300){
                  $('.main-down').fadeOut();
              }
              if ($(this).scrollTop() > dst_prod){
                  $('#label').fadeIn();
              }
              else if ($(this).scrollTop() < 100){
                  $('#label').fadeOut();
              }
              if ($(this).scrollTop() > 100){
                  $('.top-mob').addClass('fixed-menu-arr');
                  $('.menu').addClass('fixed-menu');
              }
              else if ($(this).scrollTop() < 100){
                  $('.top-mob').removeClass('fixed-menu-arr');
                  $('.menu').removeClass('fixed-menu');

              }
              //else $('#label').fadeOut();
              //noinspection JSValidateTypes,JSAnnotator
          });
      });

//    Alax

        var top_show = 150; // В каком положении полосы прокрутки начинать показ кнопки "Наверх"
    var delay = 1000; // Задержка прокрутки
    $(document).ready(function() {

         $(window).scroll(function () { // При прокрутке попадаем в эту функцию
            if ($(this).scrollTop() > top_show) $('#top').fadeIn();
            else $('#top').fadeOut();
             //noinspection JSValidateTypes,JSAnnotator
            });
        $('#top').click(function () { // При клике по кнопке "Наверх" попадаем в эту функцию
          /* Плавная прокрутка наверх */
          $('body, html').animate({scrollTop: 0}, delay);

        });

    });
    $(document).on("click", "#filter_click", function (e){
        e.preventDefault();
        console.log('aa');
        $.ajax({
            url: '/filter/',
            type: 'POST',
            data: $('#filer').serialize(),
            success: function(data){
                var html = "";
                $('#products').empty();
                $("input[name=\'filter_check\']").removeAttr("checked");
                $('.main-down').removeClass('hidden');
                if (window.location.pathname != '/'){
                    $('#products').removeClass('col-md-10').removeClass('col-md-offset-1').addClass('col-md-12');
                }
                else {

                }
                var dst_prod = $('#prod').offset().top - 300;
                html += '<h3 class="mt-10 mb-20 text-center">Товары</h3>';
                $.each(data, function(index, data) {
                    console.log(data.cont);
                    var content = data.cont;
                    content = content.replace(/<[^>]+>/g,'');
                    if (window.location.pathname == '/') {
                        html +=
                            '<div class="col-sm-6 col-md-4 col-lg-3 prod-item">'
                            + '<div class="thumbnail">'
                                +'<a href="/product/' + data.url_name + '/' + data.id + '" title="">'
                                    + '<img class="main-prod-img" src="' + data.img + '" alt="...">'
                                +'</a>'
                                + '<div class="caption text-center">'
                                    + '<h3 class="mt-10">' + data.model + '</h3>'
                                    + '<div class="prod-def">' + content + '...' + '</div>'
                                        + '<a href="/product/' + data.id + '/' + '" class="mb-20 btn-learn btn">Подробнее</a>'
                                    + '</div>'
                                + '</div>'
                            + '</div>';
                    }
                    else {
                        html +=
                            '<div class="col-sm-6 col-md-4 col-lg-3 prod-item">'
                            + '<div class="thumbnail">'
                                +'<a href="/product/' + data.url_name + '/' + data.id + '" title="">'
                                    + '<img class="main-prod-img" src="' + data.img + '" alt="...">'
                                +'</a>'
                                + '<div class="caption text-center">'
                                    + '<h3 class="mt-10">' + data.model + '</h3>'
                                    + '<div class="prod-def">' + content + '...' + '</div>'
                                        + '<a href="/product/' + data.id + '/' + '" class="mb-20 btn-learn btn">Подробнее</a>'
                                    + '</div>'
                                + '</div>'
                            + '</div>';
                    }
                    });
                $('#products').append(html);
                $('body, html').animate({scrollTop: dst_prod}, 500);
                }
            });
        });
    $('#feed').submit(function (e) {
        e.preventDefault();
        console.log('aa');
        $.ajax({
            url: '/mess/',
            type: 'POST',
            data: $('#feed').serialize(),
            success: function (data) {
                var html = "";
                $('.modal-body').empty();
                html += '<div class="text-center"'
                    + '<p>'
                    + 'Спасибо!'
                    + '<br/>'
                    + 'Ожидайте звонок консультанта'
                    + '</p>'
                    + '</div>';
                $('.modal-body').append(html);
            }
        });
    });
     $('#gem_mess').click(function (e){
         if ($('.masseger-group').css('display') == 'none')
         {
             $('.masseger-group').css("display", "block");
             $('#telegram').fadeIn(400).css("display", "block");
             $('#viber').fadeIn(600).css("display", "block");
             $('#whatsapp').fadeIn(800).css("display", "block");
             $('#facebook').fadeIn(1000).css("display", "block");
         }
         else {
              $('.masseger-group').fadeOut(200);
         }
     });


});


