from django.contrib import admin
from shop.models import *
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from import_export import fields
from import_export.widgets import ForeignKeyWidget
# Register your models here.


class MetatitleResource(resources.ModelResource):
    class Meta:
        model = Metatitle
        fields = [field.name for field in Metatitle._meta.fields]
        import_id_fields = [
            'id', ]  # it means that the instance could be updated if the instance with the value in this filed exists


class MetatitleAdmin(ImportExportModelAdmin):
    resource_class = MetatitleResource
    list_display = [field.name for field in Metatitle._meta.fields]
    search_fields = [field.name for field in Metatitle._meta.fields]
    list_filter = [field.name for field in Metatitle._meta.fields]


admin.site.register(Metatitle, MetatitleAdmin)


class AboutResource(resources.ModelResource):
    class Meta:
        model = About
        fields = [field.name for field in About._meta.fields]
        import_id_fields = [
            'id', ]  # it means that the instance could be updated if the instance with the value in this filed exists


class AboutAdmin(ImportExportModelAdmin):
    resource_class = AboutResource
    list_display = [field.name for field in About._meta.fields]
    search_fields = [field.name for field in About._meta.fields]
    list_filter = [field.name for field in About._meta.fields]

admin.site.register(About, AboutAdmin)


class CategoryResource(resources.ModelResource):
    class Meta:
        model = Category
        fields = [field.name for field in Category._meta.fields]
        import_id_fields = [
            'id', ]  # it means that the instance could be updated if the instance with the value in this filed exists


class CategoryAdmin(ImportExportModelAdmin):
    resource_class = CategoryResource
    list_display = [field.name for field in Category._meta.fields]
    search_fields = [field.name for field in Category._meta.fields]
    list_filter = [field.name for field in Category._meta.fields]

admin.site.register(Category, CategoryAdmin)


class SubcategoryResource(resources.ModelResource):
    class Meta:
        model = Subcategory
        fields = [field.name for field in Subcategory._meta.fields]
        import_id_fields = [
            'id', ]  # it means that the instance could be updated if the instance with the value in this filed exists


class SubcategoryAdmin(ImportExportModelAdmin):
    resource_class = SubcategoryResource
    list_display = [field.name for field in Subcategory._meta.fields]
    search_fields = [field.name for field in Subcategory._meta.fields]
    list_filter = [field.name for field in Subcategory._meta.fields]
admin.site.register(Subcategory, SubcategoryAdmin)


class ProductResource(resources.ModelResource):
    class Meta:
        model = Product
        fields = [field.name for field in Product._meta.fields]
        import_id_fields = [
            'id', ]  # it means that the instance could be updated if the instance with the value in this filed exists


class ImglistInline(admin.TabularInline):
    model = Img_product


class ProductAdmin(ImportExportModelAdmin):
    resource_class = ProductResource
    list_display = [field.name for field in Product._meta.fields]
    Product_fields = [field.name for field in Product._meta.fields]
    list_filter = [field.name for field in Product._meta.fields]
    inlines = [ImglistInline,]

admin.site.register(Product, ProductAdmin)


class Img_productResource(resources.ModelResource):
    class Meta:
        model = Img_product
        fields = [field.name for field in Img_product._meta.fields]
        import_id_fields = [
            'id', ]  # it means that the instance could be updated if the instance with the value in this filed exists


class Img_productAdmin(ImportExportModelAdmin):
    resource_class = Img_productResource
    list_display = [field.name for field in Img_product._meta.fields]
    Img_product_fields = [field.name for field in Img_product._meta.fields]
    list_filter = [field.name for field in Img_product._meta.fields]

admin.site.register(Img_product, Img_productAdmin)


class ContactsResource(resources.ModelResource):
    class Meta:
        model = Contacts
        fields = [field.name for field in Contacts._meta.fields]
        import_id_fields = [
            'id', ]  # it means that the instance could be updated if the instance with the value in this filed exists


class PhoneInline(admin.TabularInline):
    model = Phone


class EmailInline(admin.TabularInline):
    model = Email


class ContactsAdmin(ImportExportModelAdmin):
    resource_class = ContactsResource
    list_display = [field.name for field in Contacts._meta.fields]
    search_fields = [field.name for field in Contacts._meta.fields]
    list_filter = [field.name for field in Contacts._meta.fields]
    inlines = [PhoneInline, EmailInline]
admin.site.register(Contacts, ContactsAdmin)


class MetatitleResource(resources.ModelResource):
    class Meta:
        model = Metatitle
        fields = [field.name for field in Metatitle._meta.fields]
        import_id_fields = [
            'id', ]  # it means that the instance could be updated if the instance with the value in this filed exists