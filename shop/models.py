import datetime
from django.utils.timesince import timesince
from django.db import models
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from uuslug import uuslug
from uuslug import slugify


class Metatitle(models.Model):
    keywords = models.TextField(blank=True, default='')
    description = models.TextField(blank=True, default='')
    img = models.ImageField(null=True, blank=True)
    title = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return '%s' % self.title

    class Meta:
        db_table = 'metatitle'

class Banner(models.Model):
    head = models.CharField(verbose_name=u'Название', max_length=500, null=True, blank=True)
    img = models.ImageField(verbose_name=u'Картинка', null=True, blank=True)

    def __str__(self):
        return '%s' % self.head

    class Meta:
        db_table = 'banners'
        verbose_name = 'Баннер'
        verbose_name_plural = 'Баннеры'


class About(models.Model):
    head = models.CharField(max_length=500, null=True, blank=True)
    text =  RichTextUploadingField(verbose_name=u'Текст', null=True, blank=True)

    def __str__(self):
        return '%s' % self.head

    class Meta:
        db_table = 'about'
        verbose_name = 'О_нас'
        verbose_name_plural = 'О_нас'


class Category(models.Model):
    head = models.CharField(verbose_name=u'Категория', max_length=500, blank=True, default='')
    url_name = models.CharField(verbose_name=u'url_name', max_length=300, blank=True, null=True)

    def __str__(self):
        return '%s' % self.head

    class Meta:
        db_table = 'category'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def save(self, *args, **kwargs):
        self.url_name = slugify(self.head, separator='_', max_length=500)
        super(Category, self).save(*args, **kwargs)


class Subcategory(models.Model):
    category = models.ForeignKey(Category, verbose_name=u'Выбераем категорию', null=True, blank=True)
    head = models.CharField(verbose_name=u'Подкатегория', max_length=500, blank=True, default='')
    img = models.ImageField(verbose_name=u'Картинка', blank=True)
    url_name = models.CharField(verbose_name=u'url_name', max_length=300, blank=True, null=True)

    def __str__(self):
        return '%s' % self.head

    class Meta:
        db_table = 'subcategory'
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'

    def save(self, *args, **kwargs):
        self.url_name = slugify(self.head, separator='_', max_length=500)
        super(Subcategory, self).save(*args, **kwargs)


class Product(models.Model):
    category = models.ForeignKey(Category, verbose_name=u'Выбераем категорию', null=True, blank=True)
    subcategory = models.ForeignKey(Subcategory, verbose_name=u'Выбераем подкатегорию', null=True, blank=True)
    model = models.CharField(verbose_name=u'Название обязательное уникальное поле', max_length=500, null=True, blank=True)
    content = RichTextUploadingField(verbose_name=u'Описание товара', null=True, blank=True)
    main_content = RichTextUploadingField(verbose_name=u'Краткое товара для плитки', null=True, blank=True)
    url_name = models.CharField(verbose_name=u'url_name сохраняется само', max_length=300, blank=True, null=True)

    def __str__(self):
        return '%s' % self.model

    class Meta:
        db_table = 'product'
        verbose_name = 'Товар'
        verbose_name_plural = 'Товари'

    def save(self, *args, **kwargs):
        self.url_name = slugify(self.model, separator='_', max_length=500)
        super(Product, self).save(*args, **kwargs)


class Img_product(models.Model):
    product = models.ForeignKey(Product, null=True, blank=True)
    img = models.ImageField(verbose_name=u'Картинка', blank=True)
    main = models.BooleanField(verbose_name=u'Отображать для плитки только одна', default=False)

    def __str__(self):
        return '%s' % self.product

    class Meta:
        db_table = 'img_product'
        verbose_name = 'Картинка_товара'
        verbose_name_plural = 'Картинки_товаров'


class Contacts(models.Model):
    head = models.CharField(verbose_name=u'Название раздела',  max_length=1000, null=True, blank=True)
    text = models.CharField(verbose_name=u'Текст раздела', max_length=2000, null=True, blank=True)

    def __str__(self):
        return '%s' % self.head

    class Meta:
        db_table = 'Contact'
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'


class Phone(models.Model):
    contact_key = models.ForeignKey(Contacts, null=True, blank=True)
    phone = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return '%s' % self.phone

    class Meta:
        db_table = 'Phone'
        verbose_name = 'Телефон'
        verbose_name_plural = 'Телефоны'


class Email(models.Model):
    contact_key = models.ForeignKey(Contacts, null=True, blank=True)
    email = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return '%s' % self.email

    class Meta:
        db_table = 'Email'
        verbose_name = 'Email'
        verbose_name_plural = 'Emails'