# -*- coding: utf-8 -*-
import json

from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render
from shop.models import *
from django.core.mail import send_mail
from django.conf import settings
from django.views.decorators.cache import cache_page
from django.core.serializers.json import DjangoJSONEncoder
import datetime


# Create your views here.


def home(request):
    meta = Metatitle.objects.first()
    about = About.objects.first()
    category = Category.objects.all()
    subcategories = Subcategory.objects.all()
    subcategory_item = Subcategory.objects.first()
    category_item = Category.objects.first()
    first_ard = Contacts.objects.first()
    phones = Phone.objects.filter(contact_key=first_ard.id)
    email = Email.objects.filter(contact_key=first_ard.id)
    contact = Contacts.objects.first()
    return render(request, "home.html", locals())


def product_main(request, cat_url, sub_url):
    meta = Metatitle.objects.first()
    subcategory_item = Subcategory.objects.first()
    category_item = Category.objects.first()
    about = About.objects.first()
    category = Category.objects.get(url_name=cat_url)
    subcategory = Subcategory.objects.get(url_name=sub_url)
    category_list = Category.objects.all()
    subcategories_list = Subcategory.objects.all()
    product = Img_product.objects.filter(main=True, product__category__url_name=cat_url,
                                         product__subcategory__url_name=sub_url)
    first_ard = Contacts.objects.first()
    phones = Phone.objects.filter(contact_key=first_ard.id)
    email = Email.objects.filter(contact_key=first_ard.id)
    contact = Contacts.objects.first()
    return render(request, "productmain.html", locals())


def product(request, url_name, prod_id):
    meta = Metatitle.objects.first()
    about = About.objects.first()
    subcategory_item = Subcategory.objects.first()
    category_item = Category.objects.first()
    category_list = Category.objects.all()
    subcategories_list = Subcategory.objects.all()
    product_item = Product.objects.get(url_name=url_name, id=prod_id)
    img = Img_product.objects.filter(product__url_name=url_name, product__id=prod_id)
    first_ard = Contacts.objects.first()
    phones = Phone.objects.filter(contact_key=first_ard.id)
    email = Email.objects.filter(contact_key=first_ard.id)
    contact = Contacts.objects.first()

    return render(request, "product.html", locals())