# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-08-21 16:22
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('head', models.CharField(blank=True, max_length=500, null=True)),
                ('text', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Текст')),
            ],
            options={
                'verbose_name_plural': 'О_нас',
                'db_table': 'about',
                'verbose_name': 'О_нас',
            },
        ),
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('head', models.CharField(blank=True, max_length=500, null=True, verbose_name='Название')),
                ('img', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Картинка')),
            ],
            options={
                'verbose_name_plural': 'Баннеры',
                'db_table': 'banners',
                'verbose_name': 'Баннер',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('head', models.CharField(blank=True, default='', max_length=500, verbose_name='Категория')),
                ('url_name', models.CharField(blank=True, max_length=300, null=True, verbose_name='url_name')),
            ],
            options={
                'verbose_name_plural': 'Категории',
                'db_table': 'category',
                'verbose_name': 'Категория',
            },
        ),
        migrations.CreateModel(
            name='Contacts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('head', models.CharField(blank=True, max_length=1000, null=True, verbose_name='Название раздела')),
                ('text', models.CharField(blank=True, max_length=2000, null=True, verbose_name='Текст раздела')),
            ],
            options={
                'verbose_name_plural': 'Контакты',
                'db_table': 'Contact',
                'verbose_name': 'Контакты',
            },
        ),
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(blank=True, max_length=200, null=True)),
                ('contact_key', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='shop.Contacts')),
            ],
            options={
                'verbose_name_plural': 'Emails',
                'db_table': 'Email',
                'verbose_name': 'Email',
            },
        ),
        migrations.CreateModel(
            name='Img_product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(blank=True, upload_to='', verbose_name='Картинка')),
                ('main', models.BooleanField(default=False, verbose_name='Отображать для плитки только одна')),
            ],
            options={
                'verbose_name_plural': 'Картинки_товаров',
                'db_table': 'img_product',
                'verbose_name': 'Картинка_товара',
            },
        ),
        migrations.CreateModel(
            name='Metatitle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('keywords', models.TextField(blank=True, default='')),
                ('description', models.TextField(blank=True, default='')),
                ('img', models.ImageField(blank=True, null=True, upload_to='')),
                ('title', models.CharField(blank=True, max_length=500, null=True)),
            ],
            options={
                'db_table': 'metatitle',
            },
        ),
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(blank=True, max_length=200, null=True)),
                ('contact_key', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='shop.Contacts')),
            ],
            options={
                'verbose_name_plural': 'Телефоны',
                'db_table': 'Phone',
                'verbose_name': 'Телефон',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.CharField(blank=True, max_length=500, null=True, verbose_name='Модель обязательное уникальное поле')),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Описание товара')),
                ('main_content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Краткое товара для плитки')),
                ('url_name', models.CharField(blank=True, max_length=300, null=True, verbose_name='url_name сохраняется само')),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='shop.Category', verbose_name='Выбераем категорию')),
            ],
            options={
                'verbose_name_plural': 'Товари',
                'db_table': 'product',
                'verbose_name': 'Товар',
            },
        ),
        migrations.CreateModel(
            name='Subcategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('head', models.CharField(blank=True, default='', max_length=500, verbose_name='Подкатегория')),
                ('img', models.ImageField(blank=True, upload_to='', verbose_name='Картинка')),
                ('url_name', models.CharField(blank=True, max_length=300, null=True, verbose_name='url_name')),
            ],
            options={
                'verbose_name_plural': 'Подкатегории',
                'db_table': 'subcategory',
                'verbose_name': 'Подкатегория',
            },
        ),
        migrations.AddField(
            model_name='product',
            name='subcategory',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='shop.Subcategory', verbose_name='Выбераем подкатегорию'),
        ),
        migrations.AddField(
            model_name='img_product',
            name='product',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='shop.Product'),
        ),
    ]
